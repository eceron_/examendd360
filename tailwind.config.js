module.exports = {
  content: [
    "./src/**/*.{js,jsx,ts,tsx}",
  ],
  darkMode: "class",
  theme: {
    extend: {
      colors: {
        "dk-blue-800": '#262B3C',
        "dk-blue-700": '#2A3041',
        "dk-blue-600": '#565F7E',
        "dk-white": '#DADCE0',
        "dk-white-800": '#FFFFFF',
        "dk-grey-box-800": '#939B9F',
        "dk-grey-box-600": '#939b9f33',
        "dk-grey-modal-600": '#2A303D',
      },
    },
  },
  plugins: [],
}