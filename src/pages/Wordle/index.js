import React, { useState } from "react";

import Header from "@components/Header"
import Play from "@components/Play"
import ModalInfo from "@components/ModalInfo"
import ModalStadistics from "@components/ModalStadistics"

import { LS_IS_FIRST_START } from "@constants/general";

import "./style.css"

const Wordle = () => {

  const [ showModalInfo, setShowModalInfo ] = useState(() => {
    const isFirstPlay = localStorage.getItem(LS_IS_FIRST_START)
    if (!isFirstPlay) localStorage.setItem(LS_IS_FIRST_START, true)
    return !isFirstPlay
  })
  const [ showModalStadistics, setShowModalStadistics ] = useState(false)


  return (
    <section className="flex flex-col items-center justify-center wordle dark:bg-dk-blue-800">
      <Header
        toogleModalInfo={setShowModalInfo}
        toogleModalStadistics={setShowModalStadistics}
      />
      <Play
        toogleModalStadistics={setShowModalStadistics}
      />
      <ModalInfo
        visible={showModalInfo}
        closeModal={() => setShowModalInfo(false)}
      />
      <ModalStadistics
        visible={showModalStadistics}
        closeModal={() => setShowModalStadistics(false)}
      />



    </section>
  );
};

export default Wordle;
