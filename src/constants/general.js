export const DEFAULT_CHARACTERS_BY_WORD = 5
export const DEFAULT_ATTEMPTS = 5
export const DEFAULT_TIME_TO_RESTART_GAME = 300 // 5MIN
export const KEYS_ALLOWED = ["a","b","c","d","e","f","g","h","i","j","k","l","m","n","ñ","o","p","q","r","s","t","u","v","w","x","y","z","enter","backspace"];


export const LS_MATRIZ = "matriz"
export const LS_WORD = "secretWord"
export const LS_WORD_ORIGINAL = "secretWordOriginal"
export const LS_POSITION = "position"
export const LS_GAMES = "games"
export const LS_VICTORIES = "victories"
export const LS_FINISH = "finish"
export const LS_COUNTER = "counter"
export const LS_IS_FIRST_START = "firstGame"

export const LS_THEME = "theme"


