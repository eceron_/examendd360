import React from "react";

import Box from "@components/Box"

// import "./style.css"

const ListBox = ({data}) => {
  if (!data.length) return null

  const renderWords = (row, indexRow) => row.map((word, indexWord) => (
    <Box
      letter={word.letter}
      validate={word.validate}
      key={indexRow + indexWord}
    />
  ))
  
  return data.map((row, indexRow) => {
    return (
      <div className="play-row-word flex" key={indexRow}>
        {renderWords(row, indexRow)}
      </div>
    )
  })
};

export default ListBox;
