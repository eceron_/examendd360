import React, { useState, useEffect } from "react";

import { LS_THEME } from "@constants/general"

import "./style.css"

const Header = ({toogleModalInfo, toogleModalStadistics}) => {

  const [ isDarkMode, setDarkmode ] = useState(localStorage.getItem(LS_THEME) === "dark")

  useEffect(() => {
    if (!localStorage.getItem(LS_THEME)) localStorage.setItem(LS_THEME, "ligth")
    else if (localStorage.getItem(LS_THEME) === "dark") document.documentElement.classList.add("dark")
  }, [])

  const onToogleDarkMode = () => {
    if (localStorage.getItem(LS_THEME) === "ligth") {
      document.documentElement.classList.add("dark")
      setDarkmode(true)
      return localStorage.setItem(LS_THEME, "dark")
    }
    document.documentElement.classList.remove("dark")
    setDarkmode(false)
    return localStorage.setItem(LS_THEME, "ligth")
  }

  return (
    <>
      <div className="header flex justify-between items-center dark:bg-dk-blue-700">
        <div onClick={() => toogleModalInfo(true)}>
          <img className="cursor-pointer" src={isDarkMode ? "/assets/img/VectorDk.png" : "/assets/img/Vector.png"} alt="info" />
        </div>
        <div>
          <h1 className="header-title uppercase dark:text-dk-white">Wordle</h1>
        </div>
        <div className="flex justify-between items-center">
          <div onClick={() => toogleModalStadistics(true)}>
            <img className="cursor-pointer header-img-stadistics" src={isDarkMode ? "/assets/img/stadisticsDk.png" : "/assets/img/stadisticsLigth.png"} alt="stadistics" width="29.1" heigth="24" />
          </div>
          <div onClick={onToogleDarkMode}>
            <img className="cursor-pointer" src={isDarkMode ? "/assets/img/switchDk.png" : "/assets/img/switchLigth.png"} alt="stadistics" width="60" heigth="30" />
          </div>
        </div>
      </div>
    </>
  );
};

export default Header;
