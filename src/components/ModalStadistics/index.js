import React from "react";

import Timmer from "@components/Timer";

import { LS_GAMES, LS_VICTORIES, LS_FINISH, LS_WORD_ORIGINAL, LS_COUNTER } from "@constants/general"

import "./style.css"

const ModalStadistics = ({visible, closeModal}) => {

  const getWord = () => {
    return atob(localStorage.getItem(LS_WORD_ORIGINAL));
  }

  if (!visible) return null

  return (
    <div className="modal fade fixed top-0 left-0 w-full h-full outline-none overflow-x-hidden overflow-y-auto modal-bg flex justify-center items-center dark:bg-dk-grey-modal-600">
      <div className="modal-body modal-stadistics dark:bg-dk-blue-800">
        <h1 className="text-center dark:text-dk-white-800">Estadísticas</h1>
        <div className="flex justify-between">
          <div>
            <h1 className="text-center dark:text-dk-white-800">{localStorage.getItem(LS_GAMES)}</h1>
            <p className="dark:text-dk-white-800">Jugadas</p>
          </div>
          <div>
            <h1 className="text-center dark:text-dk-white-800">{localStorage.getItem(LS_VICTORIES)}</h1>
            <p className="dark:text-dk-white-800">Victorias</p>
          </div>
        </div>
        { localStorage.getItem(LS_FINISH) && (
          <>
            { localStorage.getItem(LS_FINISH) === "inccorect" && (
              <p className="text-center dark:text-dk-white-800">La palabra era: <span className="font-bold uppercase">{getWord()}</span></p>
            ) }
            <p className="text-center dark:text-dk-white-800">SIGUIENTE PALABRA</p>
            <Timmer
              className="text-center font-bold dark:text-dk-white-800"
              initialCount={localStorage.getItem(LS_COUNTER)}
            />
          </>
        )}
        <div className="text-center pt-3">
          <button type="button"
            className="inline-block button-custom button-cutom-bg-green text-white cursor-pointer"
            onClick={closeModal}
          >
            Aceptar
          </button>
        </div>
      </div>
    </div>
  );
};

export default ModalStadistics;
