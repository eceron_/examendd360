import React, { useState, useEffect } from "react";
import range from "lodash/range";
import orderBy from "lodash/orderBy";

import Box from "@components/Box";
import Keyboard from "@components/Keyboard";
import ListBox from "@components/ListBox";

import { replaceAccents } from "@utils/general";

import {
  DEFAULT_CHARACTERS_BY_WORD,
  DEFAULT_ATTEMPTS,
  LS_MATRIZ,
  LS_WORD,
  LS_WORD_ORIGINAL,
  LS_POSITION,
  LS_GAMES,
  LS_VICTORIES,
  LS_FINISH,
  LS_COUNTER,
  DEFAULT_TIME_TO_RESTART_GAME
} from "@constants/general";
import diccionario from "@constants/diccionario";

import "./style.css";

const TEMPLATE_WORD = {
  letter: "",
  validate: "",
};

const Play = ({ toogleModalStadistics }) => {
  const [matrizWordls, setMatrizWordls] = useState([]);
  const [selectWord, setSelectedWord] = useState([]);
  const [selectWordOriginal, setSelectWordOriginal] = useState("");
  const [matrizPosition, setMatrizPosition] = useState([0, 0]);
  const [alertMissingWords, setAlertMissingWords] = useState(false);

  useEffect(() => {
    if ( localStorage.getItem(LS_MATRIZ) && localStorage.getItem(LS_MATRIZ).length && localStorage.getItem(LS_WORD) ) {
      setMatrizWordls(JSON.parse(localStorage.getItem(LS_MATRIZ)));
      setSelectedWord(atob(localStorage.getItem(LS_WORD)).split(","));
      setSelectWordOriginal(atob(localStorage.getItem(LS_WORD_ORIGINAL)));
      setMatrizPosition(JSON.parse(localStorage.getItem(LS_POSITION)));

      if (localStorage.getItem(LS_COUNTER)) initTimmerToRestart(localStorage.getItem(LS_COUNTER))
    } else {
      localStorage.setItem(LS_GAMES, 0)
      localStorage.setItem(LS_VICTORIES, 0)
      initGame()
    }

  }, []);

  const initGame = () => {
    const createMatriz = range(DEFAULT_ATTEMPTS).map(
      (attemps) => {
        const createWords = range(DEFAULT_CHARACTERS_BY_WORD).map(
          (word) => {
            const template = { ...TEMPLATE_WORD };
            return template;
          }
        );
        return createWords;
      }
    );

    setMatrizWordls(createMatriz);
    selectWordRandom();
    setMatrizPosition([0,0])
  }

  useEffect(() => {
    const formatMatriz = JSON.stringify(matrizWordls);
    localStorage.setItem(LS_MATRIZ, formatMatriz);
  }, [matrizWordls]);

  useEffect(() => {
    localStorage.setItem(LS_WORD, btoa(selectWord));
  }, [selectWord]);

  useEffect(() => {
    localStorage.setItem(LS_WORD_ORIGINAL, btoa(selectWordOriginal));
  }, [selectWordOriginal]);

  useEffect(() => {
    const formatMatriz = JSON.stringify(matrizPosition);
    localStorage.setItem(LS_POSITION, formatMatriz);
  }, [matrizPosition]);

  const selectWordRandom = () => {
    const filterWordsByCharacters = diccionario.filter(
      (word) => word.length === DEFAULT_CHARACTERS_BY_WORD
    );

    const randomNumber = Math.floor(
      Math.random() * filterWordsByCharacters.length
    );

    let getSelectWord = filterWordsByCharacters[randomNumber];
    setSelectWordOriginal(getSelectWord);

    getSelectWord = replaceAccents(getSelectWord);
    getSelectWord = getSelectWord.split("");
    return setSelectedWord(getSelectWord);
  };

  const onPressLetter = (letter) => {
    const getAttemp = matrizPosition[0];
    const getPosWord = matrizPosition[1];


    if (localStorage.getItem(LS_FINISH)) return true

    if (letter === "enter") return onNextValidate();
    if (letter === "⌫") {
      const validatePosWord = getPosWord - 1 < 0 ? 0 : getPosWord - 1;
      const newMatriz = [...matrizWordls];
      newMatriz[getAttemp][validatePosWord].letter = "";

      setMatrizWordls(newMatriz);
      return setMatrizPosition([getAttemp, validatePosWord]);
    }

    // Avanzar a la siguiente posicion
    if (getPosWord === DEFAULT_CHARACTERS_BY_WORD) return true;

    const newMatriz = [...matrizWordls];
    newMatriz[getAttemp][getPosWord].letter = letter;

    setMatrizWordls(newMatriz);
    setMatrizPosition([getAttemp, getPosWord + 1]);
  };

  const onNextValidate = () => {
    const getAttemp = matrizPosition[0];
    const getPosWord = matrizPosition[1];

    if (localStorage.getItem(LS_FINISH)) return true

    if (getPosWord <= DEFAULT_CHARACTERS_BY_WORD - 1) {
      setAlertMissingWords(true);
      return setTimeout(() => setAlertMissingWords(false), 3000);
    }

    const validateMatriz = [...matrizWordls];
    let isCorrectWord = true;
    const validateAttemp = validateMatriz[getAttemp].map((word, index) => {
      let validate = "";
      if (word.letter === selectWord[index]) {
        validate = "correct-position";
      } else {
        isCorrectWord = false;
        const isExist = selectWord.find((letter) => letter === word.letter);
        validate = isExist ? "correct-letter" : "incorrect-letter";
      }

      return { ...word, validate };
    });
    validateMatriz[getAttemp] = validateAttemp;

    setMatrizWordls(validateMatriz);

    const getGames = parseInt(localStorage.getItem(LS_GAMES), 10)
    const getVictories = parseInt(localStorage.getItem(LS_VICTORIES), 10)
    if (isCorrectWord) {
      localStorage.setItem(LS_GAMES, getGames + 1)
      localStorage.setItem(LS_VICTORIES, getVictories + 1)
      localStorage.setItem(LS_FINISH, "correct")
      initTimmerToRestart()
      return toogleModalStadistics(true)
    }
    
    if (getAttemp === DEFAULT_ATTEMPTS - 1) {
      localStorage.setItem(LS_GAMES, getGames + 1)
      localStorage.setItem(LS_FINISH, "inccorect")
      initTimmerToRestart()
      return toogleModalStadistics(true)
    }
    return setMatrizPosition([getAttemp + 1, 0]);
  };


  const initTimmerToRestart = (count) => {
    let intervalId;
    const initCount = count || DEFAULT_TIME_TO_RESTART_GAME
    localStorage.setItem(LS_COUNTER, initCount)
    intervalId = setInterval(() => {
      const parse = parseInt(localStorage.getItem(LS_COUNTER), 10)
      if (parse  - 1 < 0) {
        clearInterval(intervalId)
        return restartGame()
      }
      localStorage.setItem(LS_COUNTER, parse - 1)

    }, 1000)
  }

  const restartGame = () => {
    localStorage.removeItem(LS_FINISH)
    localStorage.removeItem(LS_COUNTER)
    localStorage.removeItem(LS_WORD_ORIGINAL)
    localStorage.removeItem(LS_WORD)
    localStorage.removeItem(LS_MATRIZ)
    localStorage.removeItem(LS_POSITION)
    initGame()
    toogleModalStadistics(false)
  }

  const validateKeysKeyboard = () => {
    let distinct = []
    if (matrizWordls.length) {
      distinct = matrizWordls.reduce((accum, attemp, index) => {
        if (index <= matrizPosition[0]) {
          attemp.map(word => {
            if (word.validate) accum.push(word)
          })
        }
        return accum
      }, [])

      if (distinct.length) {
        distinct = orderBy(distinct, ["letter", "validate"])
        distinct = distinct.reduce((accum, item, index) => {
            const isExistIndex = (accum || []).findIndex(opt => opt.letter === item.letter)
            if (isExistIndex > 0) {
              if (accum[isExistIndex].validate !== "correct-position") {
                const filter = distinct.filter(opt => opt.letter === item.letter)
                const hasCorrectPosition = filter.find(opt => opt.validate === "correct-position")
                if (hasCorrectPosition) accum[isExistIndex] = hasCorrectPosition
              }
            } else {
              accum.push(item)
            }
            return accum
          }, [])
      }
    }
    return distinct
  }

  return (
    <>
      {alertMissingWords && (
        <div className="bg-neutral-600 mt-3 shadow-lg mx-auto w-96 max-w-full text-sm pointer-events-auto bg-clip-padding rounded-lg block mb-3">
          <div className="bg-neutral-600 flex justify-center items-center py-1 px-3 bg-clip-padding border-b rounded-lg">
            <p className="text-white">Acompleta la palabra</p>
          </div>
        </div>
      )}
      <div className="play-container-words">
        <ListBox data={matrizWordls} />
      </div>
      <Keyboard
        pressLetter={onPressLetter}
        validatekeys={validateKeysKeyboard()}
      />
    </>
  );
};

export default Play;
