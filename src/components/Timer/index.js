import React, { useState, useEffect } from 'react';


const Timmer = ({className, initialCount}) => {

  const [second, setSecond] = useState('00');
  const [minute, setMinute] = useState('00');
  const [counter, setCounter] = useState(initialCount);

  useEffect(() => {

    let intervalId = setInterval(() => {
      const initsecond = counter % 60;
      const initminute = Math.floor(counter / 60);
      const formatSecond = String(initsecond).length === 1 ? `0${initsecond}`: initsecond;
      const formatMinute = String(initminute).length === 1 ? `0${initminute}`: initminute;

      setSecond(formatSecond);
      setMinute(formatMinute);

      setCounter(counter => counter - 1 );
    }, 1000)
    return () => clearInterval(intervalId);
  }, [counter])


  return (
    <p className={className}>
       <span>{minute}</span>
        <span>:</span>
        <span>{second}</span>
    </p>
  )
};

export default Timmer;
