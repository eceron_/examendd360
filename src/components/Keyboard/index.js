import React, { useEffect } from "react";

import Box from "@components/Box";

import { KEYS_ALLOWED } from "@constants/general"

import { replaceAccents } from "@utils/general"

import "./style.css";

const Keyboard = ({pressLetter, validatekeys}) => {

  useEffect(() => {
    document.addEventListener("keydown", keyPress);
    return () => document.removeEventListener("keydown", keyPress);
  });

  const keyPress = (e) => {
    let keyName = event.key;
    keyName = replaceAccents(keyName)
    if (KEYS_ALLOWED.find(letter => letter === keyName)) {
      if (keyName === "enter") keyName = "enter"
      if (keyName === "backspace") keyName = "⌫"
      return pressLetter(keyName)
    }
  }


  const TEMPLATE_KEYBOARD = [
    [
      { letter: "q", validate: "", onclick: () => {pressLetter("q")  }, extraClass: "cursor-pointer keyboard-box-s" },
      { letter: "w", validate: "", onclick: () => {pressLetter("w")  }, extraClass: "cursor-pointer keyboard-box-s" },
      { letter: "e", validate: "", onclick: () => {pressLetter("e")  }, extraClass: "cursor-pointer keyboard-box-s" },
      { letter: "r", validate: "", onclick: () => {pressLetter("r")  }, extraClass: "cursor-pointer keyboard-box-s" },
      { letter: "t", validate: "", onclick: () => {pressLetter("t")  }, extraClass: "cursor-pointer keyboard-box-s" },
      { letter: "y", validate: "", onclick: () => {pressLetter("y")  }, extraClass: "cursor-pointer keyboard-box-s" },
      { letter: "u", validate: "", onclick: () => {pressLetter("u")  }, extraClass: "cursor-pointer keyboard-box-s" },
      { letter: "i", validate: "", onclick: () => {pressLetter("i")  }, extraClass: "cursor-pointer keyboard-box-s" },
      { letter: "o", validate: "", onclick: () => {pressLetter("o")  }, extraClass: "cursor-pointer keyboard-box-s" },
      { letter: "p", validate: "", onclick: () => {pressLetter("p")  }, extraClass: "cursor-pointer keyboard-box-s" },
    ],
    [
      { letter: "a", validate: "", onclick: () => {pressLetter("a")  }, extraClass: "cursor-pointer keyboard-box-s" },
      { letter: "s", validate: "", onclick: () => {pressLetter("s")  }, extraClass: "cursor-pointer keyboard-box-s" },
      { letter: "d", validate: "", onclick: () => {pressLetter("d")  }, extraClass: "cursor-pointer keyboard-box-s" },
      { letter: "f", validate: "", onclick: () => {pressLetter("f")  }, extraClass: "cursor-pointer keyboard-box-s" },
      { letter: "g", validate: "", onclick: () => {pressLetter("g")  }, extraClass: "cursor-pointer keyboard-box-s" },
      { letter: "h", validate: "", onclick: () => {pressLetter("h")  }, extraClass: "cursor-pointer keyboard-box-s" },
      { letter: "j", validate: "", onclick: () => {pressLetter("j")  }, extraClass: "cursor-pointer keyboard-box-s" },
      { letter: "k", validate: "", onclick: () => {pressLetter("k")  }, extraClass: "cursor-pointer keyboard-box-s" },
      { letter: "l", validate: "", onclick: () => {pressLetter("l")  }, extraClass: "cursor-pointer keyboard-box-s" },
      { letter: "ñ", validate: "", onclick: () => {pressLetter("ñ")  }, extraClass: "cursor-pointer keyboard-box-s" },
    ],
    [
      { letter: "enter", validate: "", onclick: () => {pressLetter("enter")  }, extraClass: "cursor-pointer keyboard-box-lg" },
      { letter: "z", validate: "", onclick: () => {pressLetter("z")  }, extraClass: "cursor-pointer keyboard-box-s" },
      { letter: "x", validate: "", onclick: () => {pressLetter("x")  }, extraClass: "cursor-pointer keyboard-box-s" },
      { letter: "c", validate: "", onclick: () => {pressLetter("c")  }, extraClass: "cursor-pointer keyboard-box-s" },
      { letter: "v", validate: "", onclick: () => {pressLetter("v")  }, extraClass: "cursor-pointer keyboard-box-s" },
      { letter: "b", validate: "", onclick: () => {pressLetter("b")  }, extraClass: "cursor-pointer keyboard-box-s" },
      { letter: "n", validate: "", onclick: () => {pressLetter("n")  }, extraClass: "cursor-pointer keyboard-box-s" },
      { letter: "m", validate: "", onclick: () => {pressLetter("m")  }, extraClass: "cursor-pointer keyboard-box-s" },
      { letter: "⌫", validate: "", onclick: () => {pressLetter("⌫")  }, extraClass: "cursor-pointer keyboard-box-lg" },
    ],
  ];

  const validateType = (letter) => {
    const isExist = validatekeys.find(word => word.letter === letter)
    if (isExist) {
      return isExist.validate}
    return ""

  }

  const renderWords = (row, indexRow) => row.map((word, indexWord) => (

  
    <Box
      letter={word.letter}
      validate={validateType(word.letter)}
      click={word.onclick}
      extraClass={word.extraClass}
      key={indexRow + indexWord}
    />
  ))
  
  const renderRow = TEMPLATE_KEYBOARD.map((row, indexRow) => {
    let isPaddingClass = ""
    if (indexRow === 0) isPaddingClass = "keyboard-row-pl-30"
    if (indexRow === 1) isPaddingClass = "keyboard-row-pl-50"
    return (
      <div className={`keyboard-row flex ${isPaddingClass}`} key={indexRow}>
        {renderWords(row, indexRow)}
      </div>
    )
  })

  return (
    <div className="keyboard dark:bg-dk-blue-700">
      {renderRow}
    </div>
  );
};

export default Keyboard;
