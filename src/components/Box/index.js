import React from "react";

import "./style.css"

const Box = ({letter, validate = "", click= () => {}, extraClass = ""}) => {

  const validateBackground = () => {
    if (validate === "correct-position") return "bg-green"
    else if (validate === "correct-letter") return "bg-yellow"
    else if (validate === "incorrect-letter") return "bg-gray dark:bg-dk-grey-box-800"
    return "dark:bg-dk-grey-box-700"
  }

  return (
    <div
      className={`box flex justify-center items-center ${validateBackground()} ${extraClass}`}
      onClick={click}
    >
      <h2 className="text-white uppercase dark:text-dk-white">{letter}</h2>
    </div>
  );
};

export default Box;
