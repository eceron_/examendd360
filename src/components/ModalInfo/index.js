import React from "react";

import ListBox from "@components/ListBox"

import "./style.css"

const example1 = [
  [{
    letter: "g",
    validate: "correct-position",
  },
  {
    letter: "a",
    validate: "",
  },
  {
    letter: "t",
    validate: "",
  },
  {
    letter: "o",
    validate: "",
  },
  {
    letter: "s",
    validate: "",
  }]
]

const example2 = [
  [{
    letter: "v",
    validate: "",
  },
  {
    letter: "o",
    validate: "",
  },
  {
    letter: "c",
    validate: "correct-letter",
  },
  {
    letter: "a",
    validate: "",
  },
  {
    letter: "l",
    validate: "",
  }]
]
const example3 = [
  [{
    letter: "c",
    validate: "",
  },
  {
    letter: "a",
    validate: "",
  },
  {
    letter: "n",
    validate: "",
  },
  {
    letter: "t",
    validate: "",
  },
  {
    letter: "o",
    validate: "incorrect-letter",
  }]
]

const ModalInfo = ({visible, closeModal}) => {
  if (!visible) return null
  return (
    <div className="modal fade fixed top-0 w-full h-full outline-none overflow-x-hidden overflow-y-auto modal-bg flex justify-center dark:bg-dk-grey-modal-600">
      <div className="modal-body modal-info overflow-scroll dark:bg-dk-blue-800">
        <h1 className="text-center dark:text-dk-white-800">Cómo jugar</h1>
        <p className="dark:text-dk-white-800">Adivina la palabra oculta en cinco intentos. </p>
        <p className="dark:text-dk-white-800">Cada intento debe ser una palabra válida de 5 letras. </p>
        <p className="dark:text-dk-white-800">Después de cada intento el color de las letras cambia para mostrar qué tan cerca estás de acertar la palabra. </p>
        <p className="font-bold dark:text-dk-white-800">Ejemplos</p>
        <div className="modal-info-pl-7">
          <ListBox
            data={example1}
          />
        </div>
        <p className="dark:text-dk-white-800">La letra G está en la palabra y en la posición correcta.</p>
        <div className="modal-info-pl-7">
          <ListBox
            data={example2}
          />
        </div>
        <p className="dark:text-dk-white-800">La letra C está en la palabra pero en la posición incorrecta.</p>
          <div className="modal-info-pl-7">
            <ListBox
              data={example3}
            />
          </div>
        <p className="pb-3 dark:text-dk-white-800">La letra O no está en la palabra.</p>
        <p className="pb-5 dark:text-dk-white-800">Puede haber letras repetidas. Las pistas son independientes para cada letra.</p>
        <p className="text-center pb-6 dark:text-dk-white-800">¡Una palabra nueva cada 5 minutos!</p>

        <div className="text-center">
          <button type="button"
            className="inline-block button-custom button-cutom-bg-green text-white uppercase cursor-pointer"
            onClick={closeModal}
          >
            !JUGAR¡
          </button>
        </div>
      </div>
    </div>
  );
};

export default ModalInfo;
