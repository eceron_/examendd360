export const replaceAccents = (word) => {
  let formatLetter = word.toLowerCase();
  formatLetter = formatLetter.replace(new RegExp(/\s/g),"");
  formatLetter = formatLetter.replace(new RegExp(/[àáâãäå]/g),"a");
  formatLetter = formatLetter.replace(new RegExp(/æ/g),"ae");
  formatLetter = formatLetter.replace(new RegExp(/ç/g),"c");
  formatLetter = formatLetter.replace(new RegExp(/[èéêë]/g),"e");
  formatLetter = formatLetter.replace(new RegExp(/[ìíîï]/g),"i");
  formatLetter = formatLetter.replace(new RegExp(/ñ/g),"n");                
  formatLetter = formatLetter.replace(new RegExp(/[òóôõö]/g),"o");
  formatLetter = formatLetter.replace(new RegExp(/œ/g),"oe");
  formatLetter = formatLetter.replace(new RegExp(/[ùúûü]/g),"u");
  formatLetter = formatLetter.replace(new RegExp(/[ýÿ]/g),"y");
  formatLetter = formatLetter.replace(new RegExp(/\W/g),"");
  return formatLetter;
};