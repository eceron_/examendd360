import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';


import Wordle from '@pages/Wordle';
import NotFound from '@pages/NotFound';

import '../styles/style.css';

const App = () => (
  <BrowserRouter>
    <Switch>
      <Route exact path="/" component={Wordle} />
      <Route component={NotFound} />
    </Switch>
  </BrowserRouter>
);

export default App;
