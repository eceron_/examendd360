const { merge } = require('webpack-merge');
const path = require('path');
const Dotenv = require('dotenv-webpack')
const common = require('./webpack.common.js');

module.exports = merge(common, {
  mode: 'development',
  devServer: {
    contentBase: path.join(__dirname, 'dist'),
    compress: true,
    port: 3000,
    open: false,
    hot: false,
    inline: false,
    liveReload: false,
    historyApiFallback: true,
  },
  plugins: [
    new Dotenv({
      path: './.env',
    })
  ],
  devtool: 'inline-source-map',
});
