# FrontEnd

Wordle es una aplicación realizada en React con Tailwind, es un juego de busqueda de palabras con un limite de oportunidades .

## Ambiente
node v16.16.0
npm 8.11.0
SO OS X


## Installation

```bash
npm install
npm start
```

La app corre por el puerto: 3000

Abrir ventana en navegador, ingresando a la siguiente url: http://localhost:3000/

